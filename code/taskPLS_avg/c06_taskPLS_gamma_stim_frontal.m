% Set up EEG PLS for a task PLS using spectral power

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data             = fullfile(rootpath, 'data');
pn.data_tfr         = fullfile(rootpath, 'data', 'tfrPowBySub');
pn.data_gamma       = fullfile(rootpath, '..', 'tfr_gamma', 'data', 'gammaPowBySub');
pn.tools            = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

%% add seed for reproducibility

rng(0, 'twister');

%% load data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = IDs(cellfun(@str2num, IDs, 'un', 1)<2000);
ageIdx{2} = IDs(cellfun(@str2num, IDs, 'un', 1)>2000);%% load data

idx_channel = 1:50;

for indGroup = 1:2
    for indID = 1:numel(ageIdx{indGroup})
        disp(['Processing ID', ageIdx{indGroup}{indID}])
        % load Gamma results
        tmp_MTM = load(fullfile(pn.data_gamma, [ageIdx{indGroup}{indID}, '_GammaMTM_Zst_v3.mat']), 'Gamma');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 3.5 & tmp_MTM.Gamma{1,indCond}.time < 6);
            freqIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.freq >= 40);
            TFRdata_MTM{indGroup}(indID,:,:,:,indCond) = squeeze(nanmean(tmp_MTM.Gamma{1,indCond}.powspctrm(idx_channel,freqIdx_MTM,timeIdx_MTM),3));
        end
    end
end

TFRdata{1} = TFRdata_MTM{1};
TFRdata{2} = TFRdata_MTM{2};

%mseavg.dat % sub*chan*scale*time*cond

time =tmp_MTM.Gamma{1}.time(timeIdx_MTM);
freq =tmp_MTM.Gamma{1}.freq(freqIdx_MTM);

num_chans = size(TFRdata{1},2);
num_freqs = size(TFRdata{1},3);
num_time = size(TFRdata{1},4);

num_subj_lst = [numel(ageIdx{1}), numel(ageIdx{2})];
num_cond = 4;
num_grp = 2;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 2; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time;

save(fullfile(pn.data, 'c06_taskpls_yaoa_gamma_stim.mat'),...
    'stat', 'result', 'lvdat', 'lv_evt_list', 'num_chans', 'num_freqs', 'num_time')
