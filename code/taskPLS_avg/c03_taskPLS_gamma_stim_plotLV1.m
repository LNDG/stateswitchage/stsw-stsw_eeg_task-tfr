% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'winsor'));
    addpath(fullfile(pn.tools, 'Cookdist'));

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load(fullfile(pn.data, 'c03_taskpls_yaoa_gamma_stim.mat'),...
    'stat', 'result', 'lvdat', 'lv_evt_list', 'num_chans', 'num_freqs', 'num_time')
load(fullfile(pn.data, 'elec'));

result.perm_result.sprob

indLV = 1;

lvdat = reshape(result.boot_result.compare_u(:,indLV), num_chans, num_freqs, num_time);
stat.prob = lvdat;
stat.mask = lvdat > 3 | lvdat < -3;

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

%% invert solution

stat.mask = stat.mask;
stat.prob = stat.prob.*-1;
result.vsc = result.vsc.*-1;
result.usc = result.usc.*-1;

%% plot multivariate topographies

h = figure('units','centimeters','position',[0 0 10 10]);
set(gcf,'renderer','Painters')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
cfg.marker = 'off'; 
cfg.highlight = 'yes';
cfg.highlightchannel = plotData.label(51:60);
cfg.highlightcolor = [0 0 0];
cfg.highlightsymbol = '.';
cfg.highlightsize = 18;
cfg.zlim = [-1 1]; 
cfg.figure = h;
plotData.powspctrm = cat(1,zeros(60-num_chans,1), ...
    squeeze(nanmean(stat.mask(:,:).*stat.prob(:,:),2))); 
ft_topoplotER(cfg,plotData);
cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');
figureName = ['c03_pls_topo_gamma_stim'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot using raincloud plot

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

%% plot RainCloudPlot (within-subject centered)

cBrew(1,:) = 2.*[.3 .1 .1];
cBrew(2,:) = 2.*[.1 .1 .3];

idx_outlier = cell(1); idx_standard = cell(1);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
    X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
    outliers = isoutlier(IndividualSlopes, 'median');
    idx_outlier{indGroup} = find(outliers);
    idx_standard{indGroup} = find(outliers==0);
end

h = figure('units','centimeter','position',[0 0 25 10]);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = dataToPlot(:,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = dataToPlot(:,i)-...
                nanmean(dataToPlot(:,:),2)+...
                repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
            data_nooutlier{i, j} = data{i, j};
            data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            data_ws_nooutlier{i, j} = data_ws{i, j};
            data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            % sort outliers to back in original data for improved plot overlap
            data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    subplot(1,2,indGroup);
    set(gcf,'renderer','Painters')
        cla;
        cl = cBrew(indGroup,:);
        [~, dist] = rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
        h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],dist);
        view([90 -90]);
        axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./1.5]);

    minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
    xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

    % test linear effect
    curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
end
figureName = ['c03_pls_rcp_gamma_stim'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% save individual brainscores & lin. modulation

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

LV1.data = cat(1,uData{1}',uData{2}');
X = [1 1; 1 2; 1 3; 1 4]; b=X\LV1.data'; 
LV1.linear = b(2,:);
LV1.IDs = IDs;

save(fullfile(pn.data, 'c03_LV1.mat'), 'LV1')
