% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'RainCloudPlots'));

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load(fullfile(pn.data, 'f01_pls.mat'), ...
    'stat', 'result', 'lvdat', 'lv_evt_list')
load(fullfile(pn.data, 'f01_pls_data.mat'), ...
    'num_chans', 'num_freqs', 'num_time')
load(fullfile(pn.data,'elec.mat'))

indLV = 1;

lvdat = reshape(result.boot_result.compare_u(:,indLV), num_chans, num_freqs, num_time);
stat.prob = lvdat;
stat.mask = lvdat > 2 | lvdat < -2;

%% invert solution

% stat.mask = stat.mask;
% stat.prob = stat.prob.*-1;
% result.vsc = result.vsc.*-1;
% result.usc = result.usc.*-1;

%% plot pupil, 1/f separately

h = figure('units','normalized','position',[.1 .1 .7 .3]);
set(gcf,'renderer','Painters')

% plot multivariate brainscores

maskNaN = double(stat.mask);
maskNaN(maskNaN==0) = NaN;

subplot(2,3,1); cla;
statsPlot = squeeze(nanmean(stat.prob([ 58:60],end,:),1));
plot(stat.time-3,statsPlot, 'k')
hold on;
statsMask = squeeze(nanmean(maskNaN([58:60],end,:),1));
statsMask(statsMask==0) = NaN; statsMask(statsMask>0) = 1;
statsPlot = statsMask.*statsPlot;
plot(stat.time-3,statsPlot, 'r', 'LineWidth', 2)
set(gca,'Ydir','Normal');
xlabel('Time [s from stim onset]'); ylabel('BSR');
xlim([-.2 3])
title({'Pupil dilation'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

subplot(2,3,2); cla;
statsPlot = squeeze(nanmean(stat.prob(:,end-1,:),3));
plot([1:60],statsPlot, 'k')
hold on;
statsMask = squeeze(nanmean(maskNaN(:,end-1,:),3));
statsMask(statsMask==0) = NaN; statsMask(statsMask>0) = 1;
statsPlot = statsMask.*statsPlot;
scatter([1:60],statsPlot, 'filled', 'r', 'LineWidth', 2)
set(gca,'Ydir','Normal');
xlabel('Channels'); ylabel('BSR');
title({'1/f'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

subplot(2,3,3); cla;
statsPlot = squeeze(nanmean(stat.prob([58:60],end-2,:),1));
plot(stat.time-3,statsPlot, 'k')
hold on;
statsMask = squeeze(nanmean(maskNaN([58:60],end-2,:),1));
statsMask(statsMask==0) = NaN; statsMask(statsMask>0) = 1;
statsPlot = statsMask.*statsPlot;
plot(stat.time-3,statsPlot, 'r', 'LineWidth', 2)
set(gca,'Ydir','Normal');
xlim([-.2 2.9])
xlabel('Time [s from stim onset]'); ylabel('BSR');
title({'SampleEntropy'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

% plot multivariate topographies

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(2,3,4); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(58:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3];
    cfg.figure = h;
    plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,end,:).*stat.prob(:,end,:),2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(2,3,5); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(58:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3];
    cfg.figure = h;
    plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,end-1,:).*stat.prob(:,end-1,:),[],2),3)); ft_topoplotER(cfg,plotData);
    %plotData.powspctrm = squeeze(nanmean(nanmax(stat.prob(:,end-1,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(2,3,6); cla;
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3];
    cfg.figure = h;
    plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,end-2,:).*stat.prob(:,end-2,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
colormap(cBrew)

set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'f01_LV1_loadings';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% initiate figure

h = figure('units','normalized','position',[.1 .1 .7 .3]);
set(gcf,'renderer','Painters')

%% plot multivariate brainscores

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

subplot(3,3,[2,5,8]);
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(10:12,1:7,:).*stat.prob(10:12,1:7,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(51:60,8:14,:).*stat.prob(51:60,8:14,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask([53:55, 58:60],15:end-3,:).*stat.prob([53:55, 58:60],15:end-3,:),1)));
imagesc(stat.time-3,[],statsPlot,[-4 4])
line([3.5 6]-3, [8 8],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([3.5 6]-3, [16 16],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
set(gca,'Ydir','Normal');
frequencies = [stat.freq(1:7), NaN, stat.freq(8:14), NaN, stat.freq(15:end-3)];
set(gca, 'YTickLabels', round(frequencies(get(gca, 'YTick')),2));
xlabel('Time [s from stim onset]'); ylabel('Frequency [Hz]');
title({'Multivariate spectral changes'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');
xlim([.5 2.9])

%% plot multivariate topographies

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(3,3,[3]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(10:12);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3]; 
    cfg.figure = h; 
    plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,1:7,:).*stat.prob(:,1:7,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(3,3,[6]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(51:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-5 5]; 
    cfg.figure = h; 
    plotData.powspctrm = squeeze(nanmean(nanmin(stat.mask(:,10:15,:).*stat.prob(:,10:15,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Min BSR');
subplot(3,3,[9]); cla;
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([53:55, 58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    %cfg.zlim = [-.1 .1]; 
    cfg.zlim = [-1 1];
    cfg.figure = h; 
    plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,25:end-3,:).*stat.prob(:,25:end-3,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');

figureName = 'f01_LV1_TFRtopos';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
    
%% get individual data

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
        end
    end

%% plot RainCloudPlot (within-subject centered)

    cBrew(1,:) = 2.*[.3 .1 .1];
    cBrew(2,:) = 2.*[.3 .1 .1];

    idx_outlier = cell(1); idx_standard = cell(1);
    for indGroup = 1:2
        dataToPlot = uData{indGroup}';
        % define outlier as lin. modulation of 2.5*mean Cook's distance
        cooks = Cookdist(dataToPlot);
    %     cooks = Cookdist(dataToPlot-...
    %         repmat(nanmean(dataToPlot,2),1,size(dataToPlot,2))+...
    %         repmat(nanmean(nanmean(dataToPlot,2),1),size(dataToPlot,1),1));
        outliers = cooks>2.5*mean(cooks);
        idx_outlier{indGroup} = find(outliers);
        idx_standard{indGroup} = find(outliers==0);
    end

    h = figure('units','centimeter','position',[0 0 25 10]);
    for indGroup = 1:2
        dataToPlot = uData{indGroup}';
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = dataToPlot(:,i);
                % individually demean for within-subject visualization
                data_ws{i, j} = dataToPlot(:,i)-...
                    nanmean(dataToPlot(:,:),2)+...
                    repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
                data_nooutlier{i, j} = data{i, j};
                data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                data_ws_nooutlier{i, j} = data_ws{i, j};
                data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                % sort outliers to back in original data for improved plot overlap
                data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        subplot(1,2,indGroup);
        set(gcf,'renderer','Painters')
            cla;
            cl = cBrew(indGroup,:);
            rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
            h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
            view([90 -90]);
            axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);

        minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
        xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
        ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

        % test linear effect
        curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    figureName = ['f01_pls_rcp'];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
        
    %% save individual brainscores & lin. modulation
    
    LV1.data = cat(1,uData{1}',uData{2}');
    X = [1 1; 1 2; 1 3; 1 4]; b=X\LV1.data'; 
    LV1.linear = b(2,:);
    LV1.IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

    save(fullfile(pn.data, 'f02_LV1.mat'), 'LV1')
    
    %% create signature-specific brainscores
    % dot product of signature-specific weights and original data

    BrainSalience = result.u(:,1); % get salience for first component
    DataX = cat(1,datamat_lst{1},datamat_lst{2}); % sub*cond x chan*freq*num_time
    BrainScore = DataX*BrainSalience; %chan*freq*num_time x 1

    % sanity-check
    BrainScore_orig = result.usc(:,1);
    figure; scatter(BrainScore,BrainScore_orig)

    % let's reshape both matrices to the chan-freq-time dim

    data_orig = reshape(DataX,size(DataX,1), 60, num_freqs, num_time);
    salience_orig = reshape(result.u(:,1),num_chans, num_freqs, num_time);

    % get only the TFR brainscore

    data_TFR = data_orig(:,1:60,1:end-3,:);
    data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
    salience_TFR = salience_orig(1:60,1:end-3,:);
    salience_TFR = reshape(salience_TFR,[],1);
    Brainscore_TFR = data_TFR*salience_TFR;

    % subdivide into theta, alpha and gamma range

    data_TFR = data_orig(:,10:12,TFR_freqs < 8,:);
    data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
    salience_TFR = salience_orig(10:12,TFR_freqs < 8,:);
    salience_TFR = reshape(salience_TFR,[],1);
    Brainscore_TFR_theta = data_TFR*salience_TFR;

    data_TFR = data_orig(:,51:60,TFR_freqs >= 8 & TFR_freqs <= 25,:);
    data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
    salience_TFR = salience_orig(51:60,TFR_freqs >= 8 & TFR_freqs <= 25,:);
    salience_TFR = reshape(salience_TFR,[],1);
    Brainscore_TFR_alpha = data_TFR*salience_TFR;

    data_TFR = data_orig(:,51:60,TFR_freqs > 25,:);
    data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
    salience_TFR = salience_orig(51:60,TFR_freqs > 25,:);
    salience_TFR = reshape(salience_TFR,[],1);
    Brainscore_TFR_gamma = data_TFR*salience_TFR;

    % get only the sampen brainscore

    data_sampen = data_orig(:,51:60,end-2,:);
    data_sampen = reshape(data_sampen,size(data_TFR,1),[]);
    salience_sampen = salience_orig(51:60,end-2,:);
    salience_sampen = reshape(salience_sampen,[],1);
    Brainscore_sampen = data_sampen*salience_sampen;

    % get only the 1/f brainscore

    data_1f = data_orig(:,1:60,end-1,1);
    data_1f = reshape(data_1f,size(data_TFR,1),[]);
    salience_1f = salience_orig(1:60,end-1,1);
    salience_1f = reshape(salience_1f,[],1);
    Brainscore_1f = data_1f*salience_1f;

    % get only the pupil brainscore

    data_pupil = data_orig(:,1,end,:);
    data_pupil = reshape(data_pupil,size(data_TFR,1),[]);
    salience_pupil = salience_orig(1,end,:);
    salience_pupil = reshape(salience_pupil,[],1);
    Brainscore_pupil = data_pupil*salience_pupil;

    groupsizes=sum(result.num_subj_lst);
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            BrainScore_TFR(indCond,:) = Brainscore_TFR(targetEntries,1);
            BrainScore_TFR_theta(indCond,:) = Brainscore_TFR_theta(targetEntries,1);
            BrainScore_TFR_alpha(indCond,:) = Brainscore_TFR_alpha(targetEntries,1);
            BrainScore_TFR_gamma(indCond,:) = Brainscore_TFR_gamma(targetEntries,1);
            BrainScore_sampen(indCond,:) = Brainscore_sampen(targetEntries,1);
            BrainScore_1f(indCond,:) = Brainscore_1f(targetEntries,1);
            BrainScore_pupil(indCond,:) = Brainscore_pupil(targetEntries,1);
            BrainScore_all(indCond,:) = BrainScore(targetEntries,1);
        end
    end

    %% plot linear brainscore modulation of different signatures
    
    X = zscore([1 1; 1 2; 1 3; 1 4],[],1);
    
    BS.TFR = zscore(BrainScore_TFR_theta,[],1); b=X\BS.TFR;
    BS.TFR_theta_linear = b(2,:);
    
    BS.TFR = zscore(BrainScore_TFR_alpha,[],1); b=X\BS.TFR;
    BS.TFR_alpha_linear = b(2,:);
    
    BS.TFR = zscore(BrainScore_TFR_gamma,[],1); b=X\BS.TFR;
    BS.TFR_gamma_linear = b(2,:);
    
    BS.sampen = zscore(BrainScore_sampen,[],1); b=X\BS.sampen;
    BS.sampen_linear = b(2,:);
    
    BS.OneOverF = zscore(BrainScore_1f,[],1); b=X\BS.OneOverF;
    BS.OneOverF_linear = b(2,:);
    
    BS.pupil = zscore(BrainScore_pupil,[],1); b=X\BS.pupil;
    BS.pupil_linear = b(2,:);
    
    BS.all = zscore(BrainScore_all,[],1); b=X\BS.all;
    BS.all_linear = b(2,:);
    
    BS.all_equalized = cat(1, BS.TFR_theta_linear, ...
        BS.TFR_alpha_linear, ...
        BS.TFR_gamma_linear, ...
        BS.sampen_linear, ...
        BS.OneOverF_linear, ...
        BS.pupil_linear);
    BS.all_equalized = squeeze(nanmean(BS.all_equalized,1));
    % this score only makes sense if it operates on standardized betas
    
    BS.IDs = LV1.IDs;
    
    idx_YA = cellfun(@str2num, LV1.IDs)<2000;
    idx_OA = cellfun(@str2num, LV1.IDs)>2000;
    
    % plot linear loadings by age group with values superimposed
    
    parameters = {'TFR_theta_linear', 'TFR_alpha_linear', 'TFR_gamma_linear',...
        'sampen_linear', 'OneOverF_linear', 'pupil_linear', 'all_linear', 'all_equalized'};
    colorm = repmat([.6 .6 .6],8,1);

    h = figure('units','normalized','position',[.1 .1 .5 .2]);
    for indParam = 1:8
        subplot(1,8,indParam)
        curParam = BS.(parameters{indParam});
        plot_data{1} = curParam(idx_YA);
        plot_data{2} = curParam(idx_OA);
        set(gcf,'renderer','Painters')
        cla;
        hold on;
        for indGroup = 1:2
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(indParam,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
            % plot individual values on top
            scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
                plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
            % test deviation from zero
            [h1, p] = ttest(plot_data{indGroup});
            text(indGroup, 0, num2str(p));
        end
        xlim([.25 2.75]); %ylim([-2 2])
        set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([parameters{indParam},' linear'])
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
        [h2, p] = ttest2(plot_data{1}, plot_data{2});
        title(['p = ', num2str(p)])
    end
    
    figureName = 'f02_signatureBS';
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');

    % save signature-specific brainscores for future assessment

    save(fullfile(pn.data, 'f02_BSlin.mat'),'BS')
    