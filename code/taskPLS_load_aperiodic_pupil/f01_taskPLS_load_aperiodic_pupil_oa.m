% Build a two age-group model, probing joint uncertainty-related changes in
% spectral power, pupil dilation, 1/f exponents, sample entropy

clear all; cla; clc;
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/[MEG]PLS/MEGPLS_PIPELINE_v2.02b'))

%% add seed for reproducibility

rng(0, 'twister');

%% main PLS part

% load data and prepare as PLS input

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/';
pn.MTMRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/O_gammaPowBySub/';
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/T_tools/fieldtrip-20170904/']);
ft_defaults

%% load data

IDs{1} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indGroup = 1
    for indID = 1:numel(IDs{indGroup})
        % load wavelet results
        tmp = load([pn.root, 'B_data/D5_TFRavg_v6/', IDs{indGroup}{indID}, '_dynamic_TFRwavePow_v6.mat'], 'TFRStruct_load_log10');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_Wavelet = find(tmp.TFRStruct_load_log10.time >= 2.5 & tmp.TFRStruct_load_log10.time < 6);
            TFRdata_Wavelet{indGroup}(indID,:,:,:,indCond) = tmp.TFRStruct_load_log10.powspctrm(indCond,:,:,timeIdx_Wavelet);
        end
        % load Gamma results
        tmp_MTM = load([pn.MTMRoot, IDs{indGroup}{indID}, '_GammaMTM_zscore_v2.mat'], 'Gamma');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 2.5 & tmp_MTM.Gamma{1,indCond}.time < 6);
            TFRdata_MTM{indGroup}(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm2(:,:,timeIdx_MTM);
        end
    end
    % concatenate wavelet and multitaper results for PLS (demean each matrix)
    
    % Note: the beta range is removed here to focus on anticipated changes in
    % theta, alpha and gamma range. Including beta-band changes in the solution
    % may create a component that rather loads on anticipatory motor
    % preparation that we want to exclude as a potential confound here.
    % i.e., we regularize the solution somewhat

    TFRdata{indGroup} = cat(3, TFRdata_Wavelet{indGroup}(:,:,1:14,:,:), TFRdata_MTM{indGroup}(:,:,15:end,:,:));
end

TFRtime = tmp_MTM.Gamma{1,indCond}.time(tmp_MTM.Gamma{1,indCond}.time >= 2.5 & tmp_MTM.Gamma{1,indCond}.time < 6);

%% attach sample entropy, 1/f slopes

pn.dataInOUT_sampen  = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/B_data/C_MSE_Output_v2/'];

SampEn{1} = load([pn.dataInOUT_sampen, 'mseavg_CSD_OA.mat'], 'mseavg');

% load example data
tmp_data = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/B_data/B_MSE_Segmented_Dim_Input/1117_dim1_MSE_IN.mat']);

time = SampEn{1}.mseavg.time;
freq = SampEn{1}.mseavg.freq;
elec = tmp_data.data.elec;

% interpolate data to spectral power time

for indChannel = 1:size(SampEn{1}.mseavg.dat,2)
    for indScale = 1:size(SampEn{1}.mseavg.dat,3)
        for indCond = 1:size(SampEn{1}.mseavg.dat,5)
            for indID = 1:size(SampEn{1}.mseavg.dat,1)
                x = SampEn{1}.mseavg.time; xq = TFRtime;
                v = squeeze(SampEn{1}.mseavg.dat(indID,indChannel,indScale,:,indCond));
                SampEn{1}.mseavg.dat_int(indID,indChannel,indScale,:,indCond) = interp1(x,v,xq);
            end
        end
    end
end


% add 1/f slopes
slopes_OA = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_SlopeFits_v3_OA.mat'], 'SlopeFits');

SampEn{1}.mseavg.dat = SampEn{1}.mseavg.dat_int(:,:,1,:,:);

% append slopes values to frequency dimension
slopesOA = repmat(permute(shiftdim(slopes_OA.SlopeFits.linFit_2_30,-2),[3,5,1,2,4]),1,1,1,size(SampEn{1}.mseavg.dat,4),1);
SampEn{1}.mseavg.dat = cat(3, SampEn{1}.mseavg.dat, slopesOA);

%% attach pupil derivatives

pn.root      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.dataInOutPupil = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];

load([pn.dataInOutPupil, 'G1_pupilDataFT.mat'], 'pupilDataFT')

tmp_TFRdata = cat(4, pupilDataFT{1}.dataDeriv, pupilDataFT{2}.dataDeriv, ...
    pupilDataFT{3}.dataDeriv, pupilDataFT{4}.dataDeriv);

PupilData{1} = squeeze(nanmean(tmp_TFRdata(48:end,:,:,:),2));

PupilTime = [pupilDataFT{1}.time(2:end)];

% interpolate data to spectral power time

for indCond = 1:size(PupilData{1},3)
    for indID = 1:size(PupilData{1},1)
        x = PupilTime; xq = TFRtime;
        v = squeeze(PupilData{1}(indID,:,indCond));
        PupilData_int(indID,:,indCond) = interp1(x,v,xq, 'linear', 'extrap');
    end
end

PupilData = [];
PupilData{1} = PupilData_int;

%% put everything together

% concatenate variables along the frequency dimension

PupilData_tmp{1} = repmat(permute(shiftdim(PupilData{1},-2),[3,1,2,4,5]),1,60,1,1,1);

TFRdata{1} = cat(3, TFRdata{1}, SampEn{1}.mseavg.dat, PupilData_tmp{1});

%% build models

%mseavg.dat % sub*chan*scale*time*cond

time = TFRtime;
freq = [tmp.TFRStruct_load_log10.freq(1:14), tmp_MTM.Gamma{1,1}.freq(15:end), 1000, 1001, 2000];
elec = tmp.TFRStruct_load_log10.elec;

% create datamat

% timeIdx = find(time > 3.5 & time < 6);
% curTFRdata = TFRdata(:,:,:,timeIdx,:);

num_chans = size(TFRdata{1},2);
num_freqs = size(TFRdata{1},3);
num_time = size(TFRdata{1},4);

num_subj_lst = [numel(IDs{1})];
num_cond = 4;
num_grp = 1;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time;

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K15_taskPLSStim_OA.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')
