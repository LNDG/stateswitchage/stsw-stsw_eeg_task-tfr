%% Spectral brainscore changes are anticorrelated with baseline brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/I2_mencemtPLS_v6_wGamma_preStim.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

BS_meancent.data = uData{indGroup};
BS_meancent.IDs = IDs; 

% estimate linear effects
X = [1 1; 1 2; 1 3; 1 4]; 
b=X\BS_meancent.data; 
BS_meancent.data_linear(:,1) = b(2,:);

%% plot anticorrelation

h = figure('units','normalized','position',[.1 .1 .2 .3]);
    cla; hold on;
    a = BS_meancent.data(1,:)'; 
    b = BS_meancent.data_linear(:,1);
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Brainscore (1)'); ylabel('Brainscore (234-1)');
    title({'Spectral brainscore changes are anticorrelated';'with baseline brainscore'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% assess relation with early ERP

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/I2_IndividualERP.mat', 'IndividualERP');

h = figure('units','normalized','position',[.1 .1 .2 .3]);
    cla; hold on;
    a = IndividualERP.data_linear(:,1);
    b = BS_meancent.data_linear(:,1);
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Prestim linear Brainscore'); ylabel('Early SSVEP modulation');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% assess relation with NDT increases
    
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

sumIDs = ismember(STSWD_summary.IDs,IDs);
    
h = figure('units','normalized','position',[.1 .1 .2 .3]);
    cla; hold on;
    a = BS_meancent.data_linear(:,1);
    b = STSWD_summary.HDDM_vt.nondecisionEEG_linear(sumIDs,1);
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Brainscore (1)'); ylabel('Brainscore (234-1)');
    title({'Spectral brainscore changes are anticorrelated';'with baseline brainscore'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
