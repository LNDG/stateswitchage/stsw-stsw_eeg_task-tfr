% visual temporal location of load effects for diverse measures
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;
ageIdx{3} = cellfun(@str2num, IDs, 'un', 1)>0;

pn.tools        = fullfile(pn.root, 'tools'); addpath(pn.tools);
pn.figures      = fullfile(pn.root, 'figures');    
pn.tfrdata      = fullfile(pn.root, 'data', 'tfrPowBySub');
pn.gammadata	= fullfile(pn.root, '..', 'tfr_gamma', 'data', 'gammaPowBySub');

addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));
addpath(fullfile(pn.tools, 'shadedErrorBar'));

% extract same channels as visualized for PLS solution

idxChanGamma = [53:55, 58:60];

%% load spectral power for theta, alpha and gamma range

TFRdata_MTM = [];
for indID = 1:numel(IDs)
    disp(['Processing ID ', IDs{indID}])
    % load Gamma results
    tmp_MTM = load(fullfile(pn.gammadata, [IDs{indID}, '_GammaMTM_Zst_v3.mat']), 'Gamma');
    for indCond = 1:4
        % restrict to the same time points
        freqIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.freq >= 40);
        TFRdata_MTM(indID,:,:,:,indCond) = ...
           tmp_MTM.Gamma{1,indCond}.powspctrm(:,freqIdx_MTM,:);
    end
end

time =tmp_MTM.Gamma{1}.time;
freq =tmp_MTM.Gamma{1}.freq(freqIdx_MTM);

TFRdata = TFRdata_MTM;

gammaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanGamma, freq> 60 & freq <150, :,:),3),2)),[1,3,2]);

%% open figure

groupnames = {'ya', 'oa', 'yaoa'};

for indGroup = 1:3
    h = figure('units','centimeters','position',[0 0 15 10]);
    cla; hold on;
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(gammaMerged(ageIdx{indGroup},:,:),2));
    curData = squeeze(gammaMerged(ageIdx{indGroup},1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(gammaMerged(ageIdx{indGroup},2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(gammaMerged(ageIdx{indGroup},3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(gammaMerged(ageIdx{indGroup},4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
    xlim([1 8]); %ylim([-.03 .18])
    ylabel({'60-150 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});    
    set(findall(gcf,'-property','FontSize'),'FontSize',12)
    
    figureName = ['b01_gammaTraces_', groupnames{indGroup}];
    h.Color = 'white';
    h.InvertHardcopy = 'off';

    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end
