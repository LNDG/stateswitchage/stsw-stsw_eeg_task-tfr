
clear all; clc;

% add path to toolbox

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/[MEG]PLS/MEGPLS_PIPELINE_v2.02b'))

%% add seed for reproducibility

rng(0, 'twister');

%% main PLS part

% load data and prepare as PLS input

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/';
pn.MTMRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/O_gammaPowBySub/';
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']);
ft_defaults

%% load data

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for indID = 1:numel(IDs)
    % load wavelet results
    tmp = load([pn.root, 'B_data/D5_TFRavg_v6/', IDs{indID}, '_dynamic_TFRwavePow_v6.mat'], 'TFRStruct_load_log10');
    for indCond = 1:4
        % restrict to the same time points
        timeIdx_Wavelet = find(tmp.TFRStruct_load_log10.time >= 2.5 & tmp.TFRStruct_load_log10.time < 6.5);
        TFRdata_Wavelet(indID,:,:,:,indCond) = squeeze(tmp.TFRStruct_load_log10.powspctrm(indCond,:,:,timeIdx_Wavelet));
    end
    % load Gamma results
    tmp_MTM = load([pn.MTMRoot, IDs{indID}, '_GammaMTM_Zst_v2.mat'], 'Gamma');
    for indCond = 1:4
        % restrict to the same time points
        timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 2.5 & tmp_MTM.Gamma{1,indCond}.time < 6.5);
        TFRdata_MTM(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm2(:,:,timeIdx_MTM);
    end
end

% concatenate wavelet and multitaper results for PLS (demean each matrix)

TFRdata = cat(3, TFRdata_Wavelet(:,:,1:14,:,:), TFRdata_MTM(:,:,15:end,:,:));

tmp.TFRStruct_load_log10.freq(1:14) % 2-15 Hz
tmp_MTM.Gamma{1,indCond}.freq(15:end)

% create baseline comparison condition by averaging -700:-100 ms prestim onset

time = tmp.TFRStruct_load_log10.time(timeIdx_Wavelet);

TFRdata_avg(:,:,:,:,1) = squeeze(nanmean(TFRdata,5));
timeIdx = find(time >= 2.3 & time < 2.9);
TFRdata_avg(:,:,:,:,2) = repmat(squeeze(nanmean(nanmean(TFRdata(:,:,:,timeIdx,:),4),5)),1,1,1,size(TFRdata_avg,4));

%mseavg.dat % sub*chan*scale*time*cond

time = tmp.TFRStruct_load_log10.time(timeIdx_Wavelet);
freq = [tmp.TFRStruct_load_log10.freq(1:14), tmp_MTM.Gamma{1,1}.freq(15:end)];
elec = tmp.TFRStruct_load_log10.elec;

% create datamat

% timeIdx = find(time > 3.5 & time < 6);
% curTFRdata = TFRdata(:,:,:,timeIdx,:);

curTFRdata = TFRdata_avg;

num_chans = size(curTFRdata,2);
num_freqs = size(curTFRdata,3);
num_time = size(curTFRdata,4);

num_subj_lst = 47;
num_cond = 2;
num_grp = 1;

datamat_lst = cell(num_grp); lv_evt_list = [];
for indGroup = 1:num_grp
    indCount = 1;
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount) = indCond;
            indCount = indCount+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time;

%% save PLS result

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/C_taskPLS_bl_v7.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')
