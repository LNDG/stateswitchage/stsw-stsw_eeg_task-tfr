% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes upon stimulus onset with spectral power during
% the baseline period.

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
pn.functions    = [pn.root, 'B_analyses/S2B_TFR_v6/T_tools/']; addpath(pn.functions);
addpath(genpath([pn.functions, 'RainCloudPlots/']))

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

%% load PLS results

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K1_taskPLS_bl_OA.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

%% initiate figure

h = figure('units','normalized','position',[.1 .1 .7 .3]);
set(gcf,'renderer','Painters')

%% plot multivariate brainscores

maskNaN = double(stat.mask);
maskNaN(maskNaN==0) = NaN;

subplot(3,3,[2,5,8]);
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(10:12,1:7,:).*stat.prob(10:12,1:7,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(44:60,8:14,:).*stat.prob(44:60,8:14,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask([53:55, 58:60],15:end,:).*stat.prob([53:55, 58:60],15:end,:),1)));
imagesc(stat.time-3,[],statsPlot,[-15 15])
line([2.5 6]-3, [8 8],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([2.5 6]-3, [16 16],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([0 0], [0 80],'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
line([3 3], [0 80],'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)

set(gca,'Ydir','Normal');
frequencies = [stat.freq(1:7), NaN, stat.freq(8:14), NaN, stat.freq(15:end)];
set(gca, 'YTickLabels', round(frequencies(get(gca, 'YTick')),2));
xlabel('Time [s from stim onset]'); ylabel('Frequency [Hz]');
title({'Multivariate spectral changes'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

%% plot multivariate topographies

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
cfg.colormap = cBrew;

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(3,3,[3]);
    cfg.marker = 'off';
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(10:12);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-10 10]; plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,2:5,:).*stat.prob(:,2:5,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar, 'location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(3,3,[6]);
    cfg.marker = 'off';
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(44:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-18 18]; plotData.powspctrm = squeeze(nanmean(nanmin(stat.mask(:,10:15,:).*stat.prob(:,10:15,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar, 'location', 'EastOutside'); set(get(cb,'ylabel'),'string','Min BSR');
subplot(3,3,[9]); 
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([53:55, 58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-8 8]; plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,25:end,:).*stat.prob(:,25:end,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar, 'location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');

%% plot loadings of latent variable

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Stimulus'; 'Baseline'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

% change up the order of the two conditions for plotting

uData{indGroup} = flip(uData{indGroup},1);
condData{indGroup} = flip(condData{indGroup},1);
conds = flip(conds,1);

% retrieve original errorbars from permutation

meanCent = result.boot_result.orig_usc(:,indLV)';
ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
llusc_meanCent = result.boot_result.llusc(:,indLV)';

meanY = [nanmean(uData{1},2)]';

ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);

ulusc = ulusc_meanCent-meanCent;
llusc = llusc_meanCent-meanCent;
errorY{1} = [llusc(1:2); ulusc(1:2)];

% condPairs = [1,2];
% condPairsLevel = [760];
% 
% subplot(3,3,[1,4,7]); cla;
% meanY = nanmean(uData{indGroup},2);    
% %errorY = nanstd(uData{indGroup},[],2)/sqrt(size(uData{indGroup},2));
% [h1, hError] = barwitherr(errorY{indGroup}', meanY);
% for indPair = 1:size(condPairs,1)
%     % significance star for the difference
%     [~, pval] = ttest(uData{indGroup}(condPairs(indPair,1),:), uData{indGroup}(condPairs(indPair,2), :)); % paired t-test
%     % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%     % sigstars on top
%     if pval <.05
%         mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%     end
% end
% set(h1(1),'FaceColor','r');
% set(h1(1),'LineWidth',2);
% set(hError(1),'LineWidth',3);
% box(gca,'off')
% set(gca, 'XTick', [1,2,3,4]);
% set(gca, 'XTickLabels', {conds{1}; conds{2}});
% ylabel({'Raw brainscore with CI'})
% xlim([.5 2.5]);
% title(['Stimulus-evoked changes (LV: p < .001)'])

%% move topoplots a bit closer

AxesHandle=findobj(h,'Type','axes');
axisPos = [];
axisPos{1} = get(AxesHandle(2),'OuterPosition'); 
axisPos{2} = get(AxesHandle(3),'OuterPosition'); 
axisPos{3} = get(AxesHandle(4),'OuterPosition'); 
set(AxesHandle(2),'OuterPosition',[axisPos{1}(1)-.1 axisPos{1}(2) axisPos{1}(3) axisPos{1}(4)]); 
set(AxesHandle(3),'OuterPosition',[axisPos{2}(1)-.1 axisPos{2}(2) axisPos{2}(3) axisPos{2}(4)]); 
set(AxesHandle(4),'OuterPosition',[axisPos{3}(1)-.1 axisPos{3}(2) axisPos{3}(3) axisPos{3}(4)]); 

cBarHandle=findobj(h,'Type','colorbar');
axisPos = [];
axisPos{1} = get(cBarHandle(1),'Position'); 
axisPos{2} = get(cBarHandle(2),'Position'); 
axisPos{3} = get(cBarHandle(3),'Position'); 
set(cBarHandle(1),'Position',[axisPos{1}(1)-.02 axisPos{1}(2) axisPos{1}(3) axisPos{1}(4)]); 
set(cBarHandle(2),'Position',[axisPos{2}(1)-.02 axisPos{2}(2) axisPos{2}(3) axisPos{2}(4)]); 
set(cBarHandle(3),'Position',[axisPos{3}(1)-.02 axisPos{3}(2) axisPos{3}(3) axisPos{3}(4)]); 

%% save Figure

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'K1_overviewPlot_baselinePLS_OA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% replace with raincloud plot

% read into cell array of the appropriate dimensions
data = [];
for i = 1:2
    for j = 1:1
        data{i, j} = uData{1}(i,:)';
    end
end

% set up figure

cl = [.6 .2 .2];

% make figure
h = figure('units','normalized','position',[.1 .1 .15 .15]); clc;
set(gcf,'renderer','Painters')

h_rc = rm_raincloud(data, cl,1);
% set(gca, 'YLim', [-0.3 1.6]);
% title(['Figure M9' newline 'Repeated measures raincloud plot']);
% add confidence intervals
% hold on;
% for indCond = 1:2
%     line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData + errorY{indGroup}(1,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
%     line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData - errorY{indGroup}(2,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
% end

% add stats
condPairs = [1,2];
condPairsLevel = [820];
for indPair = 1:size(condPairs,1)
    % significance star for the difference
    [~, pval] = ttest(uData{indGroup}(condPairs(indPair,1),:), uData{indGroup}(condPairs(indPair,2), :)); % paired t-test
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    if pval <.05
       mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

    end
end

view([90 -90]);
axis ij

set(gca, 'YTickLabels', flip({conds{1}; conds{2}})); % label assignment also has to be flipped
xlabel({'Raw brainscore with CI'})
xlim([450 850]);
yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.1, yticks(4)+(yticks(2)-yticks(1))./1.1]);
title(['Stimulus-evoked changes (LV: p < .001)'])

% check p-value for title
result.perm_result.sprob(1)

set(findall(gcf,'-property','FontSize'),'FontSize',15)

%% save Figure

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'K1_overviewPlot_baselinePLS_RCP_OA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
