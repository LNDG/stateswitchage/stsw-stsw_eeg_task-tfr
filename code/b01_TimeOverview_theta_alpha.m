% visual temporal location of load effects for diverse measures
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;
ageIdx{3} = cellfun(@str2num, IDs, 'un', 1)>0;

pn.tools        = fullfile(pn.root, 'tools'); addpath(pn.tools);
pn.figures      = fullfile(pn.root, 'figures');    
pn.tfrdata      = fullfile(pn.root, 'data', 'tfrPowBySub');
pn.gammadata	= fullfile(pn.root, '..', 'tfr_gamma', 'data', 'gammaPowBySub');

addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));
addpath(fullfile(pn.tools, 'shadedErrorBar'));

% extract same channels as visualized for PLS solution

idxChanTheta = 10:12;
idxChanAlpha = 44:60;

%% load spectral power for theta, alpha and gamma range

TFRdata_Wavelet = [];
for indID = 1:numel(IDs)
    disp(['Processing ID ', IDs{indID}])
    % load wavelet results
    load(fullfile(pn.tfrdata, [IDs{indID}, '_tfr.mat']), 'load_log10');
    for indCond = 1:4
        % restrict to the same time points
        TFRdata_Wavelet(indID,:,:,:,indCond) = load_log10.powspctrm(indCond,:,:,:);
    end
end

TFRdata = TFRdata_Wavelet;

time = load_log10.time-3;
freq = load_log10.freq;
elec = load_log10.elec;

thetaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanTheta, freq> 2 & freq <8, :,:),3),2)),[1,3,2]);
alphaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanAlpha, freq> 8 & freq <12, :,:),3),2)),[1,3,2]);

%% open figure
    
groupnames = {'ya', 'oa', 'yaoa'};

for indGroup = 1:3
    h = figure('units','centimeters','position',[0 0 15 10]);
    cla; hold on;
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(thetaMerged(ageIdx{indGroup},:,:),2));
    curData = squeeze(thetaMerged(ageIdx{indGroup},1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged(ageIdx{indGroup},2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged(ageIdx{indGroup},3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged(ageIdx{indGroup},4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
    xlim([-1.5 5]); %ylim([-.03 .18])
    ylabel({'2-8 Hz Theta Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});    
    set(findall(gcf,'-property','FontSize'),'FontSize',12)
    
    figureName = ['b01_thetaTraces_', groupnames{indGroup}];
    h.Color = 'white';
    h.InvertHardcopy = 'off';

    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end

%% plot load effect: alpha

groupnames = {'ya', 'oa', 'yaoa'};

for indGroup = 1:3
    h = figure('units','centimeters','position',[0 0 15 10]);
    cla; hold on;
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(alphaMerged(ageIdx{indGroup},:,:),2));
    curData = squeeze(alphaMerged(ageIdx{indGroup},1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(alphaMerged(ageIdx{indGroup},2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(alphaMerged(ageIdx{indGroup},3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(alphaMerged(ageIdx{indGroup},4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
    xlim([-1.5 5]); %ylim([-.03 .18])
    ylabel({'8-12 Hz Alpha Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});    
    set(findall(gcf,'-property','FontSize'),'FontSize',12)
    
    figureName = ['b01_alphaTraces_', groupnames{indGroup}];
    h.Color = 'white';
    h.InvertHardcopy = 'off';

    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end

%% plot load effect: alpha during stim

groupnames = {'ya', 'oa', 'yaoa'};

for indGroup = 1:3
    h = figure('units','centimeters','position',[0 0 15 10]);
    cla; hold on;
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(alphaMerged(ageIdx{indGroup},:,:),2));
    curData = squeeze(alphaMerged(ageIdx{indGroup},1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(alphaMerged(ageIdx{indGroup},2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(alphaMerged(ageIdx{indGroup},3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(alphaMerged(ageIdx{indGroup},4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
    xlim([0.5 3]); 
%     if indGroup ==1
%         ylim([-5.3 -5.1])
%     elseif indGroup == 2
%         
%     end
    ylabel({'8-12 Hz Alpha Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});    
    set(findall(gcf,'-property','FontSize'),'FontSize',12)
    
    figureName = ['b01_alphaTraces_stim_', groupnames{indGroup}];
    h.Color = 'white';
    h.InvertHardcopy = 'off';

    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end
