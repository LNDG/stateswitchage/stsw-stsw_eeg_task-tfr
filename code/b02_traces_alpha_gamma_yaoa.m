% visual temporal location of load effects for diverse measures

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'; ...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root  = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
addpath('/Volumes/Kosciessa/Tools/barwitherr')    
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/T_tools/') % mysigstar.m

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

% extract same channels as visualized for PLS solution

idxChanTheta = 10:12;
idxChanAlpha = 44:60;
idxChanGamma = [53:55, 58:60];

%% load spectral power for theta, alpha and gamma range

    pn.WaveletRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/';
    pn.MTMRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/O_gammaPowBySub/';

    for indID = 1:numel(IDs)
        % load wavelet results
        tmp = load([pn.WaveletRoot, 'B_data/D5_TFRavg_v6/', IDs{indID}, '_dynamic_TFRwavePow_v6.mat'], 'TFRStruct_load_bl');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_Wavelet = find(tmp.TFRStruct_load_bl.time >= 1 & tmp.TFRStruct_load_bl.time < 8);
            TFRdata_Wavelet(indID,:,:,:,indCond) = tmp.TFRStruct_load_bl.powspctrm(indCond,:,:,timeIdx_Wavelet);
        end
        % load Gamma results
        %tmp_MTM = load([pn.MTMRoot, IDs{indID}, '_GammaMTM_zscore_v2.mat'], 'Gamma');
        tmp_MTM = load([pn.MTMRoot, IDs{indID}, '_GammaMTM_ST_mean_v3.mat'], 'Gamma');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 1 & tmp_MTM.Gamma{1,indCond}.time < 8);
            %TFRdata_MTM(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm2(:,:,timeIdx_MTM);
            TFRdata_MTM(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm(:,:,timeIdx_MTM);
        end
    end
    
    TFRdata = cat(3, TFRdata_Wavelet(:,:,1:14,:,:), TFRdata_MTM(:,:,15:end,:,:));

    time = tmp.TFRStruct_load_bl.time(timeIdx_Wavelet)-3;
    freq = [tmp.TFRStruct_load_bl.freq(1:14), tmp_MTM.Gamma{1,1}.freq(15:end)];
    elec = tmp.TFRStruct_load_bl.elec;

    thetaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanTheta, freq> 2 & freq <8, :,:),3),2)),[1,3,2]);
    alphaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanAlpha, freq> 8 & freq <12, :,:),3),2)),[1,3,2]);
    gammaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanGamma, freq> 40 & freq <91, :,:),3),2)),[1,3,2]);
    
    thetaMerged_YA = thetaMerged(1:47,:,:);
    thetaMerged_OA = thetaMerged(48:end,:,:);
    
    alphaMerged_YA = alphaMerged(1:47,:,:);
    alphaMerged_OA = alphaMerged(48:end,:,:);
    
    gammaMerged_YA = gammaMerged(1:47,:,:);
    gammaMerged_OA = gammaMerged(48:end,:,:);
    
   % add within-subject error bars
    pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
 
    colBlue = [2.*[.1 .1 .2]; 6.*[.1 .1 .15]; 4.*[.1 .1 .2]; 2.*[.2 .1 .1]];
    colGrey = [2.*[.1 .1 .1]; 4.*[.1 .1 .1]; 6.*[.1 .1 .1]; 8.*[.1 .1 .1]];

%% plot Gamma power loads 1 and 4 by age group

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.05 .31];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    grandAverage = squeeze(nanmean(gammaMerged_YA(:,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged_YA(:,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
   
    curData = squeeze(nanmean(gammaMerged_YA(:,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); ylim([-.03 .3])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('Young Adults');
    
subplot(1,2,2);
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.05 .31];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    grandAverage = squeeze(nanmean(gammaMerged_OA(:,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged_OA(:,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
   
    curData = squeeze(nanmean(gammaMerged_OA(:,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

    xlabel('Time (ms); response-locked')
    xlim([-2 5]); ylim([-.03 .3])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('Older Adults');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot alpha for both age groups

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-5.2 -4.5];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    grandAverage = squeeze(nanmean(alphaMerged_YA(:,1:4,:,:),2));
    curData = squeeze(nanmean(alphaMerged_YA(:,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
   
    curData = squeeze(nanmean(alphaMerged_YA(:,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-5.2 -4.5])
    ylabel({'Alpha Power';'(a.u.; log10)'});
    xlabel({'Time (s)'});
    title('Young Adults');
    
subplot(1,2,2);
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-5.4 -4.8];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    grandAverage = squeeze(nanmean(alphaMerged_OA(:,1:4,:,:),2));
    curData = squeeze(nanmean(alphaMerged_OA(:,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
   
    curData = squeeze(nanmean(alphaMerged_OA(:,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-5.4 -4.8])
    ylabel({'Alpha Power';'(a.u.; log10)'});
    xlabel({'Time (s)'});
    title('Older Adults');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% median split based on LVs

    load([pn.root, 'B_analyses/S2B_TFR_v6/B_data/K10_LV2.mat'], 'LV2')
    load([pn.root, 'B_analyses/S2B_TFR_v6/B_data/K10_LV1.mat'], 'LV1')
    
    [~, sortInd_LV1_YA] = sort(LV1.linear(1:47),'ascend');
    sortInd_LV1_YA_bottom = sortInd_LV1_YA(1:ceil(sortInd_LV1_YA/2));
    sortInd_LV1_YA_top = sortInd_LV1_YA(ceil(sortInd_LV1_YA/2)+1:end);
    
    [tmp, sortInd_LV2_YA] = sort(LV2.linear(1:47),'ascend');
    sortInd_LV2_YA_bottom = sortInd_LV2_YA(1:ceil(sortInd_LV2_YA/2));
    sortInd_LV2_YA_top = sortInd_LV2_YA(ceil(sortInd_LV2_YA/2)+1:end);
    
    [~, sortInd_LV1_OA] = sort(LV1.linear(48:end),'ascend');
    sortInd_LV1_OA_bottom = sortInd_LV1_OA(1:ceil(sortInd_LV1_OA/2));
    sortInd_LV1_OA_top = sortInd_LV1_OA(ceil(sortInd_LV1_OA/2)+1:end);
    
    [~, sortInd_LV2_OA] = sort(LV2.linear(48:end),'ascend');
    sortInd_LV2_OA_bottom = sortInd_LV2_OA(1:ceil(sortInd_LV2_OA/2));
    sortInd_LV2_OA_top = sortInd_LV2_OA(ceil(sortInd_LV2_OA/2)+1:end);
    
    colBlue = [2.*[.1 .1 .2]; 6.*[.1 .1 .15]; 4.*[.1 .1 .2]; 2.*[.2 .1 .1]];

    h = figure('units','normalized','position',[.1 .1 .3 .2]);
    subplot(1,2,1);
        cla; hold on;
        % highlight different experimental phases in background
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-5.2 -4.5];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        
        curIDs = sortInd_LV2_YA_bottom;
        grandAverage = squeeze(nanmean(alphaMerged_YA(curIDs,1:4,:,:),2));
        curData = squeeze(nanmean(alphaMerged_YA(curIDs,1,:,:),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);

        curData = squeeze(nanmean(alphaMerged_YA(curIDs,4,:,:),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

        xlabel('Time (ms); response-locked')
        xlim([-2 5]); ylim([-5.2 -4.5])
        ylabel({'Alpha Power';'(a.u.; log10)'});
        xlabel({'Time (s)'});
        title('Young Adults');

    subplot(1,2,2);
        cla; hold on;
        % highlight different experimental phases in background
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-5.4 -4.8];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        
        curIDs = sortInd_LV2_OA_bottom;
        grandAverage = squeeze(nanmean(alphaMerged_OA(curIDs,1:4,:,:),2));
        curData = squeeze(nanmean(alphaMerged_OA(curIDs,1,:,:),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);

        curData = squeeze(nanmean(alphaMerged_OA(curIDs,4,:,:),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

        xlabel('Time (ms); response-locked')
        xlim([-2 5]); ylim([-5.4 -4.8])
        ylabel({'Alpha Power';'(a.u.; log10)'});
        xlabel({'Time (s)'});
        title('Older Adults');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% same for gamma power

colBlue = [2.*[.1 .1 .2]; 6.*[.1 .1 .15]; 4.*[.1 .1 .2]; 2.*[.2 .1 .1]];

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.05 .31];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curIDs = sortInd_LV2_YA_bottom;
    grandAverage = squeeze(nanmean(gammaMerged_YA(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged_YA(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
   
    curData = squeeze(nanmean(gammaMerged_YA(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); ylim([-.03 .3])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('Young Adults');
    
subplot(1,2,2);
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.05 .31];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curIDs = sortInd_LV2_OA_bottom;
    grandAverage = squeeze(nanmean(gammaMerged_OA(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged_OA(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
   
    curData = squeeze(nanmean(gammaMerged_OA(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

    xlabel('Time (ms); response-locked')
    xlim([-2 5]); ylim([-.03 .3])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('Older Adults');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot subject * time plots

figure; 
curSort = sortInd_LV2_YA;
subplot(2,2,1); 
    curData = squeeze(nanmean(gammaMerged_YA(curSort,1:4,:,:),2));
    imagesc(curData, [-.5 .5])
subplot(2,2,2); 
    curData = squeeze(nanmean(gammaMerged_YA(curSort,4,:,:),2))-squeeze(nanmean(gammaMerged_YA(curSort,1,:,:),2));
    imagesc(curData, [-.5 .5])
curSort = sortInd_LV2_OA;
subplot(2,2,3); 
    curData = squeeze(nanmean(gammaMerged_OA(curSort,1:4,:,:),2));
    imagesc(curData, [-.5 .5])
subplot(2,2,4); 
    curData = squeeze(nanmean(gammaMerged_OA(curSort,4,:,:),2))-squeeze(nanmean(gammaMerged_OA(curSort,1,:,:),2));
    imagesc(curData, [-.5 .5])

figure; 
curSort = sortInd_LV2_YA;
subplot(2,2,1); 
    curData = squeeze(nanmean(alphaMerged_YA(curSort,1:4,:,:),2));
    imagesc(curData)
subplot(2,2,2); 
    curData = squeeze(nanmean(alphaMerged_YA(curSort,4,:,:),2))-squeeze(nanmean(alphaMerged_YA(curSort,1,:,:),2));
    imagesc(curData, [-.25 .25])
curSort = sortInd_LV2_OA;
subplot(2,2,3); 
    curData = squeeze(nanmean(alphaMerged_OA(curSort,1:4,:,:),2));
    imagesc(curData)
subplot(2,2,4); 
    curData = squeeze(nanmean(alphaMerged_OA(curSort,4,:,:),2))-squeeze(nanmean(alphaMerged_OA(curSort,1,:,:),2));
    imagesc(curData, [-.25 .25])


%% repeat for all subjects

[~, sortInd_LV1] = sort(LV1.linear,'ascend');
[~, sortInd_LV2] = sort(LV2.linear,'ascend');

smoothPoints = 20;
    
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

figure; 
curSort = sortInd_LV2;
subplot(3,2,1); 
    curData = squeeze(nanmean(alphaMerged(curSort,1,:,:),2));
    curData = smoothts(squeeze(curData)','b',smoothPoints)';
    imagesc(zscore(curData,[],2),[-2, 2])
    ylabel('Subjects')
    title('Alpha Power L1')
subplot(3,2,2); 
    curData = squeeze(nanmean(alphaMerged(curSort,4,:,:),2))-squeeze(nanmean(alphaMerged(curSort,1,:,:),2));
    curData = smoothts(squeeze(curData)','b',smoothPoints)';
    imagesc(curData, [-.08 .08])
    ylabel('Subjects')
    title('Alpha Power L4-1')
subplot(3,2,3); 
    curData = squeeze(nanmean(gammaMerged(curSort,1,:,:),2));
    curData = smoothts(squeeze(curData)','b',smoothPoints)';
    imagesc(curData, [-.25 .25])
    ylabel('Subjects')
    title('Gamma Power L1')
subplot(3,2,4); 
    curData = squeeze(nanmean(gammaMerged(curSort,4,:,:),2))-squeeze(nanmean(gammaMerged(curSort,1,:,:),2));
    curData = smoothts(squeeze(curData)','b',smoothPoints)';
    imagesc(curData, [-.15 .15])
    ylabel('Subjects')
    title('Gamma Power L4-1')
subplot(3,2,5); 
    curData = squeeze(nanmean(thetaMerged(curSort,1,:,:),2));
    curData = smoothts(squeeze(curData)','b',smoothPoints)';
    imagesc(zscore(curData,[],2),[-2, 2])
    ylabel('Subjects')
    title('Theta Power L1')
subplot(3,2,6); 
    curData = squeeze(nanmean(thetaMerged(curSort,4,:,:),2))-squeeze(nanmean(thetaMerged(curSort,1,:,:),2));
    curData = smoothts(squeeze(curData)','b',smoothPoints)';
    imagesc(curData, [-.08 .08])
    ylabel('Subjects')
    title('Theta Power L4-1')
colormap(cBrew)

    
    %% open figure
    
%h = figure('units','normalized','position',[.1 .1 .6 .2]);
h = figure('units','normalized','position',[.1 .1 .1 .3]);

%% plot load effect: theta
    
subplot(3,1,3); 
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-6 -4];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(thetaMerged_OA,2));
    curData = squeeze(thetaMerged_OA(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged_OA(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged_OA(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged_OA(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); ylim([-5.77, -5.6])
    ylabel({'2-8 Hz Theta Power';'(a.u., log10)'});
    xlabel({'Time (s)'});
    %title({'Theta power load modulation'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',13)
    
%% plot load effect: alpha

h = figure('units','normalized','position',[.1 .1 .3 .3]);

alphaMerged_YA = alphaMerged(1:47,:,:);
alphaMerged_OA = alphaMerged(48:end,:,:);
colBlue = [8.*[.1 .1 .12]; 6.*[.1 .1 .15]; 4.*[.1 .1 .2]; 2.*[.1 .1 .2]];
colGrey = [8.*[.1 .1 .1]; 6.*[.1 .1 .1]; 4.*[.1 .1 .1]; 2.*[.1 .1 .1]];

        cla; hold on;
        % highlight different experimental phases in background
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-6 -4];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        % new value = old value ? subject average + grand average

        curData = squeeze(nanmean(alphaMerged_YA,2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
        
        curData = squeeze(nanmean(alphaMerged_OA,2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
                xlabel('Time (ms); response-locked')
        xlim([-.5 3.2]); ylim([-5.5, -4.8])
        ylabel({'8-12 Hz Alpha Power';'(a.u., log10)'});
        xlabel({'Time (s from stimulus onset)'});
        set(findall(gcf,'-property','FontSize'),'FontSize',13)

        Alpha_timeAvg_YA = squeeze(nanmean(alphaMerged_YA(:,:,time>=0 & time <= 3),3));
        Alpha_timeAvg_OA = squeeze(nanmean(alphaMerged_OA(:,:,time>=0 & time <= 3),3));

        figure; bar([squeeze(nanmean(zscore(Alpha_timeAvg_OA,[],2),1)), squeeze(nanmean(zscore(Alpha_timeAvg_YA,[],2),1))])
%     
        
%% plot load effect: alpha (md split of OA by drift L1)

alphaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,44:60, freq> 8 & freq <15, :,:),3),2)),[1,3,2]);

h = figure('units','normalized','position',[.1 .1 .3 .6]);
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
ageIndex{1} = 1:47; ageIndex{2} = 48:numel(IDs);
for indAge = 1:2
    % perform a median split
    idx_summary = ismember(STSWD_summary.IDs,IDs(ageIndex{indAge}));
    [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftEEG(idx_summary,1),'ascend');
    sortIdx = ageIndex{indAge}(sortIdx);
    idx_lowdrift = sortIdx(1:ceil(numel(sortIdx)/2));
    idx_highdrift = sortIdx(ceil(numel(sortIdx)/2)+1:end);
    subplot(2,1,indAge); cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-6 -4];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end

    curData = squeeze(nanmean(alphaMerged(idx_lowdrift,1,:),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    low = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(nanmean(alphaMerged(idx_highdrift,1,:),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    high = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .2 .2],'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .2 .2],'linewidth', 2}, 'patchSaturation', .1);

    legend([low.mainLine, high.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-5.5, -4])
    ylabel({'8-12 Hz Alpha Power';'(a.u., log10)'});
    xlabel({'Time (s from stimulus onset)'});
    set(findall(gcf,'-property','FontSize'),'FontSize',13)
end

%% plot Gamma data

h = figure('units','normalized','position',[.1 .1 .3 .6]);
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
ageIndex{1} = 1:47; ageIndex{2} = 48:numel(IDs);
for indAge = 1:2
    % perform a median split
    idx_summary = ismember(STSWD_summary.IDs,IDs(ageIndex{indAge}));
    [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftEEG(idx_summary,1),'ascend');
    sortIdx = ageIndex{indAge}(sortIdx);
    idx_lowdrift = sortIdx(1:ceil(numel(sortIdx)/2));
    idx_highdrift = sortIdx(ceil(numel(sortIdx)/2)+1:end);
    subplot(2,1,indAge); cla; hold on;
    % highlight different experimental phases in background
%     patches.timeVec = [0 3];
%     patches.colorVec = [1 .95 .8];
%     for indP = 1:size(patches.timeVec,2)-1
%         YLim = [-6 -4];
%         p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
%                     [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
%         p.EdgeColor = 'none';
%     end

    curData = squeeze(nanmean(gammaMerged(idx_lowdrift,1,:),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    low = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(nanmean(gammaMerged(idx_highdrift,1,:),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    high = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .2 .2],'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .2 .2],'linewidth', 2}, 'patchSaturation', .1);

    legend([low.mainLine, high.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-.1 .25])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s from stimulus onset)'});
    set(findall(gcf,'-property','FontSize'),'FontSize',13)
end

%% same for theta

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
ageIndex{1} = 1:47; ageIndex{2} = 48:numel(IDs);
for indAge = 1:2
    % perform a median split
    idx_summary = ismember(STSWD_summary.IDs,IDs(ageIndex{indAge}));
    [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftEEG(idx_summary,1),'ascend');
    sortIdx = ageIndex{indAge}(sortIdx);
    idx_lowdrift = sortIdx(1:ceil(numel(sortIdx)/2));
    idx_highdrift = sortIdx(ceil(numel(sortIdx)/2)+1:end);
    subplot(2,1,indAge); cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-6 -4];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end

    curData = squeeze(nanmean(thetaMerged(idx_lowdrift,1,:),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    low = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(nanmean(thetaMerged(idx_highdrift,1,:),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    high = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .2 .2],'linewidth', 2}, 'patchSaturation', .1);
    shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .2 .2],'linewidth', 2}, 'patchSaturation', .1);

    legend([low.mainLine, high.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); ylim([-6 -5])
    ylabel({'Theta Power';'(a.u.; normalized)'});
    xlabel({'Time (s from stimulus onset)'});
    set(findall(gcf,'-property','FontSize'),'FontSize',13)
end


%% WIP
h = figure('units','normalized','position',[.1 .1 .3 .3]);

alphaMerged_YA = gammaMerged(1:47,:,:);
alphaMerged_OA = gammaMerged(48:end,:,:);
colBlue = [8.*[.1 .1 .12]; 6.*[.1 .1 .15]; 4.*[.1 .1 .2]; 2.*[.1 .1 .2]];
colGrey = [8.*[.1 .1 .1]; 6.*[.1 .1 .1]; 4.*[.1 .1 .1]; 2.*[.1 .1 .1]];

        cla; hold on;
        % highlight different experimental phases in background
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-.05 .31];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        curData = squeeze(nanmean(alphaMerged_YA(:,1,:,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
        
        curData = squeeze(nanmean(alphaMerged_OA(:,4,:,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
                
        curData = squeeze(nanmean(alphaMerged_OA(:,1,:,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
       
        curData = squeeze(nanmean(alphaMerged_YA(:,4,:,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
       
        xlabel('Time (ms); response-locked')
    xlim([-.5 3.2]); ylim([-.03 .25])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    %title({'Gamma power load modulation'; ''})

    % Gamma shows similar inversion:
    
    % average gamma during stimulus presentation
    
   Gamma_timeAvg_YA = squeeze(nanmean(alphaMerged_YA(:,:,time>=0 & time <= 3),3));
   Gamma_timeAvg_OA = squeeze(nanmean(alphaMerged_OA(:,:,time>=0 & time <= 3),3));
   
   figure; bar([squeeze(nanmean(Gamma_timeAvg_OA,1)), squeeze(nanmean(Gamma_timeAvg_YA,1))])

% 
%     %% save Figure
%     
%     set(findall(gcf,'-property','FontSize'),'FontSize',12)
%     
%     pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
%     figureName = 'F_TimeOverview_OA';
%     h.Color = 'white';
%     h.InvertHardcopy = 'off';
% 
%     saveas(h, [pn.plotFolder, figureName], 'fig');
%     saveas(h, [pn.plotFolder, figureName], 'epsc');
%     saveas(h, [pn.plotFolder, figureName], 'png');
