% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools	= [pn.root, 'B_analyses/S2B_TFR_v6/T_tools/']; addpath(pn.tools);
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/T_tools/fieldtrip-20170904/']); ft_defaults;
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
pn.functions    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/T_tools/'; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]); % requires mysigstar_vert!
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/RainCloudPlots'))

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K9_taskPLSStim_YA_OA.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

indLV =2;

lvdat = reshape(result.boot_result.compare_u(:,indLV), 60, 36, 50);
stat.prob = lvdat;
stat.mask = lvdat > 3 | lvdat < -3;

%% initiate figure

h = figure('units','normalized','position',[.1 .1 .7 .3]);
set(gcf,'renderer','Painters')

%% plot multivariate brainscores

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

subplot(3,3,[2,5,8]);
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(10:12,1:7,:).*stat.prob(10:12,1:7,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(44:60,8:14,:).*stat.prob(44:60,8:14,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask([53:55, 58:60],15:end,:).*stat.prob([53:55, 58:60],15:end,:),1)));
imagesc(stat.time-3,[],statsPlot,[-4 4])
line([3.5 6]-3, [8 8],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([3.5 6]-3, [16 16],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
set(gca,'Ydir','Normal');
frequencies = [stat.freq(1:7), NaN, stat.freq(8:14), NaN, stat.freq(15:end)];
set(gca, 'YTickLabels', round(frequencies(get(gca, 'YTick')),2));
xlabel('Time [s from stim onset]'); ylabel('Frequency [Hz]');
title({'Multivariate spectral changes'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

%% plot multivariate topographies

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(3,3,[3]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(10:12);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3]; plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,1:7,:).*stat.prob(:,1:7,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(3,3,[6]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(44:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-5 5]; plotData.powspctrm = squeeze(nanmean(nanmin(stat.mask(:,10:15,:).*stat.prob(:,10:15,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Min BSR');
subplot(3,3,[9]); cla;
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([53:55, 58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3]; plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,25:end,:).*stat.prob(:,25:end,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');

%% move topoplots a bit closer

%     AxesHandle=findobj(h,'Type','axes');
%     axisPos = [];
%     axisPos{1} = get(AxesHandle(2),'OuterPosition'); 
%     axisPos{2} = get(AxesHandle(3),'OuterPosition'); 
%     axisPos{3} = get(AxesHandle(4),'OuterPosition'); 
%     set(AxesHandle(2),'OuterPosition',[axisPos{1}(1)-.1 axisPos{1}(2) axisPos{1}(3) axisPos{1}(4)]); 
%     set(AxesHandle(3),'OuterPosition',[axisPos{2}(1)-.1 axisPos{2}(2) axisPos{2}(3) axisPos{2}(4)]); 
%     set(AxesHandle(4),'OuterPosition',[axisPos{3}(1)-.1 axisPos{3}(2) axisPos{3}(3) axisPos{3}(4)]); 
% 
%     cBarHandle=findobj(h,'Type','colorbar');
%     axisPos = [];
%     axisPos{1} = get(cBarHandle(1),'Position'); 
%     axisPos{2} = get(cBarHandle(2),'Position'); 
%     axisPos{3} = get(cBarHandle(3),'Position'); 
%     set(cBarHandle(1),'Position',[axisPos{1}(1)-.02 axisPos{1}(2) axisPos{1}(3) axisPos{1}(4)]); 
%     set(cBarHandle(2),'Position',[axisPos{2}(1)-.02 axisPos{2}(2) axisPos{2}(3) axisPos{2}(4)]); 
%     set(cBarHandle(3),'Position',[axisPos{3}(1)-.02 axisPos{3}(2) axisPos{3}(3) axisPos{3}(4)]); 
    
%% plot using raincloud plot

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
        end
    end

    subplot(3,3,[1,4,7]); cla;
    %h = figure('units','normalized','position',[.1 .1 .15 .2]);
    for indGroup = 1:2
        %subplot(1,2,indGroup)
        set(gcf,'renderer','Painters')
        curData = uData{indGroup}';

         % read into cell array of the appropriate dimensions
            data = []; data_ws = [];
            for i = 1:4
                for j = 1:1
                    data{i, j} = squeeze(curData(:,i));
                    % individually demean for within-subject visualization
                    data_ws{i, j} = curData(:,i)-...
                        nanmean(curData(:,:),2)+...
                        repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
                end
            end

            % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

            if indGroup ==1
                cl = 2.*[.3 .1 .1];
            elseif indGroup ==2
                cl = 2.*[.1 .1 .3];
            end

            box off
            hold on;
            h_rc = rm_raincloud(data, cl,1);
            view([90 -90]);
            axis ij
            box(gca,'off')
            %set(gca, 'YTick', [1,2,3,4]);
            set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
            ylabel('Target load'); xlabel({'Brainscore (a.u.)'})
            %title('1/f slope modulation'); 
            set(findall(gcf,'-property','FontSize'),'FontSize',20)
            %xlim([200 325]); 
            %curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
    end

    figureName = 'K10_LV2_topo';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% plot RainCloudPlot (within-subject centered)

    h = figure('units','normalized','position',[.1 .1 .3 .2]);
    for indGroup = 1:2
        subplot(1,2,indGroup)
        set(gcf,'renderer','Painters')
        curData = uData{indGroup}';
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = squeeze(curData(:,i));
                % individually demean for within-subject visualization
                data_ws{i, j} = curData(:,i)-...
                    nanmean(curData(:,:),2)+...
                    repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
            end
        end
        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!
        if indGroup ==1
            cl = 2.*[.3 .1 .1];
        elseif indGroup ==2
            cl = 2.*[.1 .1 .3];
        end
        box off
        hold on;
        h_rc = rm_raincloud(data_ws, cl,1);
        view([90 -90]);
        axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        ylabel('Target load'); xlabel({'Brainscore (a.u.)'})
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
        xlim([180 260])
        curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
        % assess linear effect
        curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        legend(['M:', num2str(mean(IndividualSlopes)), '; p=', num2str(p)], 'location', 'SouthEast')
    end
    suptitle('ws-centered')
    
    figureName = 'K10_LV2_wscentered';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% plot RainCloudPlot (original)

    h = figure('units','normalized','position',[.1 .1 .3 .2]);
    for indGroup = 1:2
        subplot(1,2,indGroup)
        set(gcf,'renderer','Painters')
        curData = uData{indGroup}';
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = squeeze(curData(:,i));
                % individually demean for within-subject visualization
                data_ws{i, j} = curData(:,i)-...
                    nanmean(curData(:,:),2)+...
                    repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
            end
        end
        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!
        if indGroup ==1
            cl = 2.*[.3 .1 .1];
        elseif indGroup ==2
            cl = 2.*[.1 .1 .3];
        end
        box off
        hold on;
        h_rc = rm_raincloud(data, cl,1);
        view([90 -90]);
        axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        ylabel('Target load'); xlabel({'Brainscore (a.u.)'})
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
        xlim([160 280]); 
        curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
        % assess linear effect
        curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        legend(['M:', num2str(mean(IndividualSlopes)), '; p=', num2str(p)], 'location', 'SouthEast')
    end
    suptitle('original')
    
    %% save individual brainscores & lin. modulation
    
    LV2.data = cat(1,uData{1}',uData{2}');
    X = [1 1; 1 2; 1 3; 1 4]; b=X\LV2.data'; 
    LV2.linear = b(2,:);
    LV2.IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

    save([pn.root, 'B_analyses/S2B_TFR_v6/B_data/K10_LV2.mat'], 'LV2')
