% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
pn.functions    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/T_tools/'; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]); % requires mysigstar_vert!
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/RainCloudPlots'))

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/D_taskPLSStim_v7.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

%% ID set

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% plot multivariate brainscores

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(10:12,1:7,:).*stat.prob(10:12,1:7,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(44:60,8:14,:).*stat.prob(44:60,8:14,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask([53:55, 58:60],15:end,:).*stat.prob([53:55, 58:60],15:end,:),1)));
imagesc(stat.time-3,[],statsPlot,[-4 4])
line([3.5 6]-3, [8 8],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([3.5 6]-3, [16 16],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
set(gca,'Ydir','Normal');
frequencies = [stat.freq(1:7), NaN, stat.freq(8:14), NaN, stat.freq(15:end)];
set(gca, 'YTickLabels', round(frequencies(get(gca, 'YTick')),2));
xlabel('Time [s from stim onset]'); ylabel('Frequency [Hz]');
title({'Multivariate spectral changes'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'label'),'string','Mean BSR');

colormap(cBrew)

set(findall(gcf,'-property','FontSize'),'FontSize',15)

SourceData = statsPlot;
SourceData_freq = frequencies';
SourceData_time = stat.time'-3;

%% save Figure

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'M2_overviewPlot_loadPLS_BrainscoreLoadings';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot loadings of latent variable

% indLV = 1;
% 
% groupsizes=result.num_subj_lst;
% conditions=lv_evt_list;
% conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
% condData = []; uData = [];
% for indGroup = 1
%     if indGroup == 1
%         relevantEntries = 1:groupsizes(1)*numel(conds);
%     elseif indGroup == 2
%         relevantEntries = groupsizes(1)*numel(conds)+1:...
%              groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
%     end
%     for indCond = 1:numel(conds)
%         targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
%         condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
%         uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
%     end
% end
% 
% % retrieve original errorbars from permutation
% 
% meanCent = result.boot_result.orig_usc(:,indLV)';
% ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
% llusc_meanCent = result.boot_result.llusc(:,indLV)';
% 
% meanY = [nanmean(uData{1},2)]';
% 
% ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
% llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);
% 
% ulusc = ulusc_meanCent-meanCent;
% llusc = llusc_meanCent-meanCent;
% errorY{1} = [llusc(1:4); ulusc(1:4)];
% %errorY{2} = [llusc(5:end); ulusc(5:end)];
% 
% groups = {'Young adults'};
% 
% condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
% condPairsLevel = [222 224 230 226 230,230];
% 
% subplot(3,3,[1,4,7]); cla;
% meanY = nanmean(uData{indGroup},2);    
% %errorY = nanstd(uData{indGroup},[],2)/sqrt(size(uData{indGroup},2));
% [h1, hError] = barwitherr(errorY{indGroup}', meanY);
% for indPair = 1:size(condPairs,1)
%     % significance star for the difference
%     [~, pval] = ttest(uData{indGroup}(condPairs(indPair,1),:), uData{indGroup}(condPairs(indPair,2), :)); % paired t-test
%     % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%     % sigstars on top
%     if pval <.05
%         mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%     end
% end
% set(h1(1),'FaceColor','r');
% set(h1(1),'LineWidth',2);
% set(hError(1),'LineWidth',3);
% box(gca,'off')
% set(gca, 'XTick', [1,2,3,4]);
% set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
% xlabel('Target load')
% ylabel({'Raw brainscore with CI'})
% ylim([150 240]);
% title(['Load-related changes (LV: p < .001)'])

%% plot using raincloud plot

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

% retrieve original errorbars from permutation

meanCent = result.boot_result.orig_usc(:,indLV)';
ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
llusc_meanCent = result.boot_result.llusc(:,indLV)';

meanY = [nanmean(uData{1},2)]';

ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);

ulusc = ulusc_meanCent-meanCent;
llusc = llusc_meanCent-meanCent;
errorY{1} = [llusc(1:4); ulusc(1:4)];
%errorY{2} = [llusc(5:end); ulusc(5:end)];


% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = uData{1}(i,:)';
        % individually demean for within-subject visualization
        data_ws{i, j} = uData{1}(i,:)'-...
            nanmean(uData{1}(i,:)',2)+...
            repmat(nanmean(nanmean(uData{1}(i,:)',2),1),size(uData{1}(i,:)',1),1);
    end
end


% read into cell array of the appropriate dimensions
% data = [];
% for i = 1:4
%     for j = 1:1
%         data{i, j} = uData{1}(i,:)';
%     end
% end

% set up figure

cl = [.6 .2 .2];

% make figure
h = figure('units','normalized','position',[.1 .1 .15 .15]); clc;
set(gcf,'renderer','Painters')
h_rc = rm_raincloud(data, cl,1);
% set(gca, 'YLim', [-0.3 1.6]);
% title(['Figure M9' newline 'Repeated measures raincloud plot']);
% add confidence intervals
hold on;
for indCond = 1:4
    line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData + errorY{indGroup}(1,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
    line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData - errorY{indGroup}(2,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
end

% add stats
% condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
% condPairsLevel = [250 250 250 255 255,260];
condPairs = [1,2];
condPairsLevel = [255];
for indPair = 1:size(condPairs,1)
    % significance star for the difference
    [~, pval] = ttest(uData{indGroup}(condPairs(indPair,1),:), uData{indGroup}(condPairs(indPair,2), :)); % paired t-test
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    if pval <.05
       mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

    end
end

view([90 -90]);
axis ij

% test linear effect
X = [1 1; 1 2; 1 3; 1 4]; b=X\uData{1}; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);


set(gca, 'YTickLabels', flip({'1'; '2'; '3'; '4'})); % label assignment also has to be flipped
ylabel('Target load')
xlabel({'Brainscores'})
xlim([160 265]);
title(['Load-related changes (linear: p ~ 0)'])

% check p-value for title
result.perm_result.sprob(1)

set(findall(gcf,'-property','FontSize'),'FontSize',15)

%% save Figure

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'M2_overviewPlot_loadPLS_Loadings';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% source data

SourceData = uData{1}';

%% plot spectrum at frontal channels

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(nanmax(stat.prob(10:12,1:7,:),[],3),1)));
hold on; plot(stat.freq(1:7),statsPlot,'-o', 'Color', 'k', 'LineWidth', 2); xlim([2 8])
line([2 8], [3 3], 'Color', 'k', 'LineStyle', '--')
xlabel('Frequency [Hz]'); ylabel('Max. BSR during STIM')
set(findall(gcf,'-property','FontSize'),'FontSize',15)
title({'Frontal delta/theta task PLS'})
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'C3_taskPLS_DeltaTheta';

saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot frontal spectrum (requires 1/f output structure)

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .125 .2]); hold on;
curData = squeeze(nanmean(nanmean(log10(MergedPow(:,:,10:12,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar([],nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25)
set(gca, 'XTick', [1:4:28]);
xlim([7 28]); set(gca, 'XTickLabels', round(freq.freq(get(gca, 'XTick')),1));
xlabel('Frequency (Hz)'); ylabel('Power (log10; Hz)');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'C3_taskPLS_DeltaThetaFreq';

saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');