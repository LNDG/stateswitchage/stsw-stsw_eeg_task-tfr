%% Spectral brainscore changes are anticorrelated with baseline brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K3_taskPLSStim_OA.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

BS_meancent.data = uData{indGroup};
BS_meancent.IDs = IDs; 

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K6_meancentPLS_OA.mat', 'BS_meancent')

%% plot anticorrelation

h = figure('units','normalized','position',[.1 .1 .2 .3]);
    cla; hold on;
    a = BS_meancent.data(1,:); a =a';
    b = squeeze(nanmean(BS_meancent.data(2:4,:),1))-BS_meancent.data(1,:); a =a';
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Brainscore (1)'); ylabel('Brainscore (234-1)');
    title({'Spectral brainscore changes are anticorrelated';'with baseline brainscore'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
